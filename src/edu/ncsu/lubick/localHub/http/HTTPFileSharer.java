package edu.ncsu.lubick.localHub.http;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.json.JSONException;
import org.json.JSONObject;

import edu.ncsu.lubick.localHub.WebQueryInterface;

public class HTTPFileSharer extends AbstractHandler {

	private static final Logger logger = Logger.getLogger(HTTPFileSharer.class);
	private static final String PARAM_RECIPIENT = "recipient";
	private static final String PARAM_TOOLNAME = "toolName";
	private WebQueryInterface databaseLink;
	
	public HTTPFileSharer(WebQueryInterface wqi)
	{
		this.databaseLink = wqi;
	}
	
	@Override
	public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		baseRequest.setHandled(true);
		if ("POST".equals(baseRequest.getMethod()))
		{
			try
			{
				handlePost(request);
			}
			catch (JSONException e)
			{
				logger.error("JSON Exception!");
				response.getWriter().println("There was a problem in sharing file");
				response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			}
		}
		else 
		{
			logger.error("Invalid POST request while sharing file");
			response.getWriter().println("Sorry, Nothing at this URL");
		}
		
	}

	private void handlePost(HttpServletRequest request) throws JSONException
	{
		JSONObject jobj = HTTPUtils.getRequestJSON(request).getJSONObject("data");

		
		logger.debug(jobj);
		
		if (jobj == null) {
			logger.info("no data, so cancelling");
			return;
		}
		
		String recipient = jobj.optString(PARAM_RECIPIENT);
		String toolName = jobj.optString(PARAM_TOOLNAME);		

		if (recipient.isEmpty())
		{
			logger.info("recipient = "+recipient+", so cancelling");
			return;
		}
		
		logger.info("Calling shareFile functionrecipient="+recipient+" tool name="+toolName);		
		this.databaseLink.shareFileWithUser(toolName, recipient);
	
	}


}
