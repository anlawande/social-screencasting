package edu.ncsu.lubick.localHub;



public interface LocalHubProcess {
	
	boolean isRunning();
	void shutDown();
}
