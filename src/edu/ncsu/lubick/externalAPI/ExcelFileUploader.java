package edu.ncsu.lubick.externalAPI;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import edu.ncsu.lubick.localHub.UserManager;
import edu.ncsu.lubick.localHub.http.HTTPUtils;

public class ExcelFileUploader {

	private static final Logger logger = Logger.getLogger(ExcelFileUploader.class);

	private CloseableHttpClient client = HttpClients.createDefault();

	private UserManager userManager = null;
		
	public ExcelFileUploader(UserManager userManager)
	{
		this.userManager = userManager;
	}
	
	private String[] getFileForToolName (String toolName)
	{
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpGet httpGet = new HttpGet("http://localhost:5001/share?toolName="+toolName);
		
		CloseableHttpResponse response1=null;
		
		String[] files = new String[2];
		
		try {
			response1 = httpclient.execute(httpGet);
		} catch (ClientProtocolException e1) {
			logger.error("Getting file from Excel Server failed due to ClientProtocolException");
		} catch (IOException e1) {
			logger.error("Getting file from Excel Server failed due to IOException");
			e1.printStackTrace();
		}
		
		try {
			
		    HttpEntity entity = response1.getEntity();
		    BufferedReader reader = new BufferedReader(new InputStreamReader(entity.getContent()));
	        String line = null;
	        StringBuilder sb = new StringBuilder();
	        while((line = reader.readLine())!=null)
	        {
	        	if(line.equals("xls")||line.equals("xlsx"))
	        		files[1] = line;
	        	else
	        		sb.append(line);
	        }
	        
	        files[0]=sb.toString();
		    EntityUtils.consume(entity);
		
		} catch (IOException e) {
			logger.error("Error while processing received file");
		} finally {
		    try {
				response1.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return files;
	}
	
	
	public void uploadToolUsage(String toolName, String recepient)
	{
		
		String file[] = getFileForToolName(toolName);
		
		HttpPost postRequest=null;
		
		try{
		
			JSONObject jobj = new JSONObject();
			jobj.put("recipient", recepient);
			jobj.put("toolName", toolName);
			
			if(file[0]!=null && file[1]!=null)
			{
				jobj.put("file_data", file[0]);
				jobj.put("file_ext", file[1]);
			}
			
			URI putUri = HTTPUtils.buildExternalHttpURI("/fileShare");
			postRequest = new HttpPost(putUri);
			HTTPUtils.addAuth(postRequest, this.userManager);
			
			StringEntity input = new StringEntity(jobj.toString());
			logger.info("Object being sent "+ jobj.toString());
			input.setContentType("application/json");
			postRequest.setEntity(input);
			 
			HttpResponse response = client.execute(postRequest);
			 
			String responseString = HTTPUtils.getResponseBody(response);
			logger.trace(responseString);
			JSONObject responseObj = new JSONObject(responseString);
			if (!"OK".equals(responseObj.getString("_status"))) {
				logger.info("There was a problem reporting toolusage event "+jobj+" response: "+responseObj.toString(2));
			}
		
		}catch (URISyntaxException e) {
			logger.error("Problem reporting tool info",e);
		} catch (JSONException e) {
			logger.error("Problem reporting tool info",e);
		} catch (UnsupportedEncodingException e) {
			logger.error("Problem reporting tool info",e);
		}catch (IOException e)
		{
			logger.error("Problem reporting tool info",e);
		}
		finally {
			postRequest.releaseConnection();
		}

		
	}
	
}